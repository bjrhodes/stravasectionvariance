/* use to set fixtures path in specs that require them */
jasmine.getFixtures().fixturesPath = 'tests/js/fixtures/';

function safeLoadFixture(fixture) {
    loadFixtures(fixture);
    if($("#jasmine-fixtures").html().trim() === ""){
        throw "fixture " + fixture + " does not exist";
    }
};

jasmine.Matchers.prototype.toBeFunction = function() {
    return Object.prototype.toString.call(this.actual)==='[object Function]';
};

function createMocks(mockDetails){
    var mocks = {};

    mockDetails.forEach(function(mock){
        mocks[mock.name] = jasmine.createSpyObj(mock.name, mock.methods);
    });

    return mocks;
};

function resetMocks(mocks, mockDetails){
    mockDetails.forEach(function(mock){
        mock.methods.forEach(function(methodName){
            mocks[mock.name][methodName].reset();
        });
    });
};
