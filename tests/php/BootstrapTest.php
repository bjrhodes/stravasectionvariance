<?php
namespace Acme;

/**
 * Author: James
 */
class BootstrapTest extends \PHPUnit_Framework_TestCase
{
    protected $Object;

    public function setup()
    {
        $this->Object = new Bootstrap();
    }
    public function testMethodExists()
    {
        $this->assertTrue(method_exists($this->Object, 'go'));
    }
}
