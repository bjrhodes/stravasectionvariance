#!/usr/bin/php
<?php

$root = __DIR__ . '/..';
$templatesPages = $root . '/src/html/templates-pages/*.html';
$publicHtml     = $root . '/server/public_html/';

$header = file_get_contents($root . '/src/html/templates-common/header.html');
$footer = file_get_contents($root . '/src/html/templates-common/footer.html');
$count = 0;

foreach (glob($templatesPages) as $page) {
    // replace all space-characters with spaces and trim the page name to forma title
    $title = trim(ucwords(str_replace(array('_', '-', '.html', 'index'), ' ', basename($page))));
    if ($title) {
        $myHeader = preg_replace(
            '/<title>(.*)<\/title>/', '<title>${1} | ' . $title . '</title>',
            $header
        );
    } else {
        $myHeader = $header;
    }
    $name = $publicHtml . basename($page);
    touch($name);
    file_put_contents($name, $myHeader . file_get_contents($page) . $footer);
    ++$count;
}

echo "generated $count pages.";
