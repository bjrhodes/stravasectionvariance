project_type = :stand_alone
output_style = :expanded
cache_path   = "build/.sass-cache"

sass_dir         = "src/sass"
images_dir       = "src/images"
sprite_load_path = "src/sass/sprites"
css_dir          = "build/client/css/"
relative_assets  = false
line_comments    = false

http_images_path           = "/assets/images/"
http_fonts_path            = "/assets/fonts/"
http_generated_images_path = "/assets/images/"
