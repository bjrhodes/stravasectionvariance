module.exports = {
  client: 'build/client/',
  server: 'build/server',
  coverage: ['build/coverage', 'build/js-coverage'],
  all: ['build/client/','build/server','build/coverage', 'server/public_html']
}
