module.exports = {
    // Two js files. One for all the app files. The other to conditionally help out IE9 or below
    /*
     * To include html5 in your app add the following in your header, after CSS:
     *    <!--[if lt IE 9]>
     *      <script src="assets/html5shiv.min.js"></script>
     *   <![endif]-->
     *
     */
  js: {files: [{src: ['build/client/js/main.min.js'], dest: 'server/public_html/assets/main.min.js'}]},
  conditionals: {files: [{expand: true, cwd: 'src/js/conditional/', src: ['**'], dest: 'server/public_html/assets/ie/'}]},
  css: {files: [{src: ['build/client/css/style.min.css'], dest: 'server/public_html/assets/style.min.css'}]},
  images: {files: [{expand: true, cwd: 'build/client/images-compressed/', src: ['**'], dest: 'server/public_html/assets/images/'}]},
  fonts: {files: [{expand: true, cwd: 'src/fonts/', src: ['**'], dest: 'server/public_html/assets/fonts/'}]},
  php: {files: [{expand: true, cwd: 'src/php/public_html/', src: ['**'], dest: 'server/public_html/'}]}
}
