module.exports = {
  scss: {
    files: ['src/sass/**/*'],
    tasks: ['buildcss']
  },
  jssrc: {
    files: ['src/js/**/*'],
    tasks: ['testjs', 'buildjs']
  },
  css: {
    files: ['build/client/css/style.min.css'],
    tasks: ['copy:css']
  },
  js: {
    files: ['build/client/js/main.min.js'],
    tasks: ['copy:js']
  },
  images: {
    files: ['src/images/**/*'],
    tasks: ['imagemin']
  },
  minifiedimages: {
    files: ['build/client/images-compressed/**/*'],
    tasks: ['copy:image']
  },
  php: {
    files: ['src/php/**/*.php', '!**/Autoload.php'],
    tasks: ['buildphp']
  },
  phpunit: {
    files: ['tests/php/**/*.php', '!**/Autoload.php'],
    tasks: ['testphp']
  },
  staticfiles: {
    files: ['src/html/**/*'],
    tasks: ['buildstatics']
  }
}
