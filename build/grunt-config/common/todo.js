module.exports = {
  options: {
    marks: [
      {
        pattern: "todo",
        color: "yellow"
      },
      {
        pattern: /console\../,
        color: "pink"
      }
    ]
  },
  src: ['src/**/*', '!**/vendor/**/*']
}
