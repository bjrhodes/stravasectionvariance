module.exports = {
  composer: {
    options: {
      stdout: true,
      stderr: true,
      failOnError: true
    },
    command: 'composer install'
  },
  staticcontent: {
    options: {
      stdout: true,
      failOnError: true,
      execOptions: {
        cwd: 'build'
      }
    },
    command: './build-pages.sh'
  }
}
