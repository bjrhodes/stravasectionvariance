module.exports = {
  application: {dir: 'src/php'},
  options: {
    reportFormat: 'text',
    exclude: ['vendor'],
    rulesets: 'build/phpmd.xml',
    bin: 'src/php/vendor/bin/phpmd'
  }
}
