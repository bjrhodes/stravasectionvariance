module.exports = {
  tests: ['tests/php/**/*.php'],
  app: ['src/php/app/**/*.php'],
  all: ['src/php/app/**/*.php', 'tests/php/**/*.php']
}
