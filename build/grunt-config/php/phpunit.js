module.exports = {
  classes: {
    dir: 'tests/php'
  },
  options: {
    configuration: 'tests/php/phpunit-no-coverage.xml',
    colors: true,
    stopOnFailure: true,
    followOutput: true,
    bin: 'src/php/vendor/bin/phpunit'
  }
}
