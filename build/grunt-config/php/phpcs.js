module.exports = {
  application: {dir: ['src/php']},
  options: {
    standard: 'build/PHPCS/ruleset.xml',
    ignore: 'vendor',
    bin: 'src/php/vendor/bin/phpcs'
  }
}
