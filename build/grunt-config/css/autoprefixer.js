module.exports = {
  options: {
    // eg, last 2 versions and anything over 1% usage globally and ie8 and ie7
    browsers: ['last 2 version', '> 1%', 'ff >= 4', 'safari >= 4', 'ie 8', 'ie 7', "bb 10", "android 4"]
  },
  all : {
    files: {
      'build/client/css/prefixed.css': ['build/client/css/style.css']
    }
  }
}
