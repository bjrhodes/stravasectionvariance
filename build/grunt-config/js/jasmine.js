module.exports = {
  control: {
    src: 'src/js/src/**/*.js',
    options: {
      specs: 'tests/js/spec/**/*Spec.js',
      helpers: [
        'tests/js/helpers/jasmine-jquery.js',
        'tests/js/helpers/specHelper.js'
      ],
      vendor: 'src/js/vendor/**/*.js',
      // code coverage
      template: require('grunt-template-jasmine-istanbul'),
      templateOptions: {
        coverage: 'build/js-coverage/coverage.json',
        report: [
          {
            type: 'html',
            options: {
              dir: 'build/js-coverage'
            }
          },
          {
            type: 'text-summary'
          }
        ]
      }
    }
  }
}
