module.exports = {
  all: {
    src: ['src/js/src/**/*.js', 'src/js/main.js'],
    tests: 'tests/js/spec/**/*Spec.js',
    options: {
      curly: true,
      eqeqeq: true,
      immed: true,
      latedef: true,
      newcap: true,
      noarg: true,
      sub: true,
      undef: true,
      unused: true,
      boss: true,
      eqnull: true,
      browser: true,
      globals: {
        jQuery: true,
        $: true
      }
    }
  }
}
