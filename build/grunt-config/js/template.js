module.exports = {
  js: {
    src: 'src/js/templates/**/*.mustache',
    dest: 'build/client/js/templates.js',
    variables: {
      name: 'templates',
      staticPath: 'src/js/templates'
    }
  }
}
