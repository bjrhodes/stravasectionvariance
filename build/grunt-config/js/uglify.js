module.exports = {
  options: {
    banner: '<%= meta.banner %>'
  },
  main: {
    files: {
      'build/client/js/main.min.js': ['build/client/js/cat.js']
    }
  }
}
