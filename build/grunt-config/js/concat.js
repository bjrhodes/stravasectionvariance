module.exports = {
  options: {
    banner: '<%= meta.banner %>'
  },
  all: {
    src: [
      'build/client/js/templates.js',
      'src/js/vendor/**/*.js',
      'src/js/src/**/*.js',
      'src/js/main.js'
    ],
    dest: 'build/client/js/cat.js'
  }
}
