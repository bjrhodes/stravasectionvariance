module.exports = {
  sass: {
    options: {
        config: 'build/compass-config.rb',
        sassDir: 'src/sass',
    }
  }
}
