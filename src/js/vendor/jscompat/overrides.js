/**
 *  Environment overrides for backwards compatibility.
 *
 */

String.prototype.capitalise = String.prototype.capitalise || function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

if(!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g,'');
  };
}
/**
* Will shorten the string to max OR 13 char if max is less than 13.
* @param max int a max string length
*/
if(!String.prototype.shorten) {
  String.prototype.shorten = function (max) {

  	var start, end, l;

  	max = (max && Math.max(max, 13)) || 13;

  	l = this.length;

    if(l <= max) {
    	return this.toString();
    }

	// calculate a sensible end portion length: Will use 25% of the string for the end, or 20 characters if the string is super-long, 5 charcters if string is short
	end = Math.max(5, Math.min(Math.floor(max * 0.25), 20));

	// subtract the end portion and periods length to get the length of the start
	start = (max - end) - 3;

	// truncate start and end and concat into new string
	return this.substr(0,start) + '...' + this.substr(l - end);
  };
}

if ( !Array.prototype.forEach ) {
  Array.prototype.forEach = function(fn, scope) {
    for(var i = 0, len = this.length; i < len; ++i) {
      fn.call(scope, this[i], i, this);
    }
  };
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        "use strict";
        if (this == null) {
            throw new TypeError();
        }
        var t = Object(this);
        var len = t.length >>> 0;
        if (len === 0) {
            return -1;
        }
        var n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            } else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        var k = n >= 0 ? n : Math.max(len - Math.abs(n), 0);
        for (; k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    }
}

if (!Array.prototype.some)
{
  Array.prototype.some = function(fun /*, thisp */)
  {
    "use strict";

    if (this == null)
      throw new TypeError();

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun != "function")
      throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++)
    {
      if (i in t && fun.call(thisp, t[i], i, t))
        return true;
    }

    return false;
  };
}
