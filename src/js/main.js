$('.nav a').each(function(k,v){
    if ($(v).get(0).href === window.location.href) {
        $(v).addClass('current');
    }
});

var $imgReplace = $('img.primed-capcha');
if ($imgReplace.size()) {
    // prime a capcha and display the capcha image by replacing the placeholder src with the real one.
    $.get($imgReplace.attr('data-uri'), function(){
        $imgReplace.attr('src', $imgReplace.attr('data-src'));
    });
}

$('li.login a').click(function(e){
    e.preventDefault();
    $('.login-popover').toggle();
});

$('a.tip').click(function(e){
    e.preventDefault();
});
$('a.tip').hover(function(){
    $('.signup-tip-hover').show();
}, function(){
    $('.signup-tip-hover').hide();
});
