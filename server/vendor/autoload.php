<?php
/**
 * This is provided to run the local install.
 *
 * In your deployment phase, copy your vendor and php app from the /src/php
 * directory
 */
require __DIR__ . "/../../src/php/vendor/autoload.php";
