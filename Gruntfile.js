/**
 * AIM: we want 3 different configs here.
 *  One for developing things using watch which should produce unminified, concated output
 *  One to build for development which produces minified concat output
 *  One to build for production which produces versioned, minified concat, gzipped output.
 *
 *  Should, in all three, build and (if appropriate) version scss too.
 */

/*global module:false*/
module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt, {pattern: ['grunt-*', '!*istanbul'], scope: 'devDependencies'});

  function loadConfig(path) {
    var glob = require('glob');
    var object = {};
    var key;

    glob.sync('*.js', {cwd: path}).forEach(function(option) {
      key = option.replace(/\.js$/,'');
      object[key] = require(path + option);
    });

    return object;
  }

  var pkg = grunt.file.readJSON('package.json');
  pkg.version = grunt.option('packageversion') || pkg.version;

  grunt.initConfig({pkg: pkg});

  // include just what you need from the list...
  grunt.extendConfig(loadConfig('./build/grunt-config/common/'));
  grunt.extendConfig(loadConfig('./build/grunt-config/compass/'));
  grunt.extendConfig(loadConfig('./build/grunt-config/css/'));
  grunt.extendConfig(loadConfig('./build/grunt-config/js/'));
  grunt.extendConfig(loadConfig('./build/grunt-config/php/'));

  grunt.registerTask('init', ['clean:all', 'shell:composer', 'default']);

  grunt.registerTask('testjs', ['jshint', 'jasmine']);
  grunt.registerTask('buildjs', ['template:js', 'concat:all', 'uglify:main', 'copy:js', 'copy:conditionals']);

  grunt.registerTask('buildcss', ['imagemin', 'compass', 'autoprefixer', 'cssmin', 'copy:css', 'copy:images', 'copy:fonts']);

  grunt.registerTask('buildphp', ['copy:php']);
  grunt.registerTask('testphp', ['phplint', 'phpunit', 'phpcs', 'phpmd']);

  grunt.registerTask('buildstatics', ['shell:staticcontent']);

  // example large app
  grunt.registerTask('default', ['testphp', 'buildphp', 'buildstatics', 'testjs', 'buildjs', 'buildcss', 'todo'])
};
